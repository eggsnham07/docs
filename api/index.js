const { readFileSync, readFile, fstat } = require("fs")
const { app_root } = require("../config")

const app = require("express")()
var docsInfo = JSON.parse(readFileSync(__dirname + "/docs.json"))

console.log(docsInfo)

/* docsInfo structure:
    {
        ":blob": [
            {
                "id": ":slug",
                "methods": [
                    "name": "SomeName",
                    "arguments": "number:int, string:String",
                    "returns": "void"
                ]
            }
        ]
    }
*/

app.get("/api/docs.json", (req, res) => {
    res.setHeader("content-type", "application/json")
    res.send(readFileSync(__dirname + "/docs.json", "utf-8"))
})

app.get("/RunNGun/:blob", (req, res) => {
    const { blob } = req.params
    if(docsInfo[blob]) {
        var body = readFileSync(app_root + "/pages/template.html", "utf-8")

        body = body
            .replace(/\{DOCS-TITLE\}/g, blob)
            .replace(/\{PROPERTIES\}/g, "")

        var newBody = "<h3>Objects:</h3><ul>"
        docsInfo[blob].forEach(object => {
            newBody += `<li><a href="${req.url}/${object.id}">${object.id}</a></li>`
        })
        newBody += "</ul>"

        body = body.replace(/\{METHODS\}/g, newBody)

        res.send(body)
    }
})

app.get("/", (req, res) => {
    res.send(readFileSync(app_root + "/pages/index.html", "utf-8"))
})

app.get("/RunNGun", (req, res) => {
    res.send(readFileSync(app_root + "/pages/RunNGun.html", "utf-8"))
})

app.get("/RunNGun/:blob/:slug", (req, res) => {
    const { blob, slug } = req.params
    var body = readFileSync(app_root + "/pages/template.html", "utf-8")
    console.log(docsInfo, "\n", blob)

    body = body.replace(/\{DOCS-TITLE\}/g, slug)

    docsInfo[blob].forEach(object => {
        if(!object.id == slug || slug == undefined) return res.status(404)
        if(object["methods"]) {
            const methods = `<h3>Methods</h3><code><table><tr><th>Returns</th><th>Method</th></tr>${genTables(object.methods, "methods")}</table></code>`
            body = body.replace(/\{METHODS\}/g, methods)
        }

        if(object["properties"]) {
            const properties = `<h3>Properties</h3><code><table><tr><th>Type</th><th>Propertie</th></tr>${genTables(object.properties, "properties")}</table></code>`
            body = body.replace(/\{PROPERTIES\}/g, properties)
        }
    })

    res.send(body)

    function genTables(array, type) {
        //if(!Array.isArray(array)) throw new TypeError("Error in function 'genTables(array)'\n   Array.isArray(array) was false")

        var tables = ""

        if(type == "methods") { 
            array.forEach(table => {
                tables += `<tr><td>${table["returns"]}</td><td>${table["name"]}(${table["arguments"]})</td></tr>`
            })
        }
        else if(type == "properties") {
            array.forEach(table => {
                tables += `<tr><td>${table["type"]}</td><td>${table["name"]}</td></tr>`
            })
        }
        else {
            throw new TypeError("type was not defined")
        }

        return tables
    }
})

module.exports = app